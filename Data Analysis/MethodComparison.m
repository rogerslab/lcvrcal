% Method Comparison2

wavelengths=500:10:680;
theta=[0, 45];
figure(1)
hold on
for i=wavelengths
    if i<540
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths.mat'])
        fftdata = fft(data,size(data,1),1);         
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));
        
        radphase0(wavelengths==i,:)=phase(:,1,theta==0);
        radphase45(wavelengths==i,:)=phase(:,1,theta==45);
        
        lambdasint(wavelengths==i)=lambda;
        allvoltages(wavelengths==i,:)=voltages;
        
    elseif i==540
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths.mat'])
        fftdata = fft(data,size(data,1),1);
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));

        radphase0(wavelengths==i,:)=phase(:,1,theta==0);
        radphase45(wavelengths==i,:)=phase(:,1,theta==45);
        
        lambdasint(wavelengths==i)=lambda;
        allvoltages(wavelengths==i,:)=voltages;
        
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths (2).mat'])
        fftdata = fft(data,size(data,1),1);
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));

        radphase0(find(wavelengths==i)+1,:)=phase(:,1,theta==0);
        radphase45(find(wavelengths==i)+1,:)=phase(:,1,theta==45);
        
        lambdasint(find(wavelengths==i)+1)=lambda;
        allvoltages(find(wavelengths==i)+1,:)=voltages;
        
     elseif i>540
         
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths.mat'])
        fftdata = fft(data,size(data,1),1);
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));

        radphase0(find(wavelengths==i)+1,:)=phase(:,1,theta==0);
        radphase45(find(wavelengths==i)+1,:)=phase(:,1,theta==45);
        
        lambdasint(find(wavelengths==i)+1)=lambda;
        allvoltages(find(wavelengths==i)+1,:)=voltages;
    end
end
nmphase0=lambdasint'*ones(1,size(allvoltages,2)).*radphase0/(2*pi);
nmphase45=lambdasint'*ones(1,size(allvoltages,2)).*radphase45/(2*pi);

%% 
%cutoff=550;
%exclude=[560];
% wavelengthindex=[1:length(lambdas)];
% %[~,i]=intersect(lambdas,exclude);
% wavelengthindex(i)=[];
% wavelengthindex(lambdas<cutoff)=[];
% 
% num=length(wavelengthindex);

%% 
d=zeros(20,133-41);

for t=41:133
    d(:,t-40)=(radphase45(:,t+1)-radphase45(:,t))./(allvoltages(:,t+1)-allvoltages(:,t));
end
    [v,ix]=max(abs(d),[],2);
    ix=ix+40;

figure(1)
hold on
rgbRainbow = hsv(300);
rgbRainbow = flipud(rgbRainbow(1:251,:));
lambdasColorsint=zeros(length(lambdasint),3); 

for i=1:length(lambdasint)
    if lambdasint(i) <= 400
        lambdasColorsint(i,:) = rgbRainbow(1,:);
    elseif lambdasint(i) >= 650
        lambdasColorsint(i,:) = rgbRainbow(251,:);
    else
        lambdasColorsint(i,:) = rgbRainbow(round(lambdasint(i))-399,:);
    end
    radphase0adj(i,:)=radphase0(i,:)+(pi-radphase0(i,ix(i)));
    nmphase0adj(i,:)=nmphase0(i,:)+(lambdasint(i)/2-nmphase0(i,ix(i)));
end

load('LCVRxpoldataSUPERK_alltwo.mat', 'voltages', 'intensities', 'lambdas')
intensities(6,1)=NaN;

lambdasColors=zeros(length(lambdas),3); 

v=find(voltages==2.3);    
[max1, max1ix]=max(intensities(:,1:v),[],2);
[min1, min1ix]=min(intensities(:,1:v),[],2);    
[max2, max2ix]=max(intensities(:,v:end),[],2);    
[min2,min2ix]=min(intensities(:,v:end),[],2);
max2ix=max2ix+v-1;
min2ix=min2ix+v-1;

for n=1:length(lambdas)
    if n<7
        normIntensities(n,:)=[(intensities(n,1:min1ix(n))-min1(n))./(max1(n)-min1(n)),...
    (intensities(n,min1ix(n)+1:max2ix(n))-min1(n))./(max2(n)-min1(n)),...
    (intensities(n,max2ix(n)+1:end)-min2(n))./(max2(n)-min2(n))];
radwrapped(n,:)=real(2*acos(sqrt(normIntensities(n,:))));
rad(n,:)=[radwrapped(n,1:max1ix(n))+2*pi,-radwrapped(n,max1ix(n)+1:min1ix(n))+2*pi,...
    radwrapped(n,min1ix(n)+1:max2ix(n)),-radwrapped(n,max2ix(n)+1:min2ix(n)),...
    radwrapped(n,min2ix(n)+1:end)-2*pi]+pi;
    else
            normIntensities(n,:)=[(intensities(n,1:max2ix(n))-min1(n))./(max2(n)-min1(n)),...
    (intensities(n,max2ix(n)+1:end)-min2(n))./(max2(n)-min2(n))];
radwrapped(n,:)=real(2*acos(sqrt(normIntensities(n,:))));
rad(n,:)=[-radwrapped(n,1:min1ix(n))+2*pi, radwrapped(n,min1ix(n)+1:max2ix(n)),...
    -radwrapped(n,max2ix(n)+1:min2ix(n)), radwrapped(n,min2ix(n)+1:end)-2*pi]+pi;
    end
    if lambdas(n) <= 400
        lambdasColors(n,:) = rgbRainbow(1,:);
    elseif lambdas(n) >= 650
        lambdasColors(n,:) = rgbRainbow(251,:);
    else
        lambdasColors(n,:) = rgbRainbow(round(lambdas(n))-399,:);
    end
end

plotlambdas=[560:20:680];

for i=plotlambdas
    inx=find(i==lambdasint);
    int=plot(allvoltages(inx,:),radphase0adj(inx,:),'Color',lambdasColorsint(inx,:));

    inx=find(i==lambdas);
    xp=plot(voltages,rad(inx,:), 'Color', 'k');%lambdasColors(inx,:));% *lambdas(inx)/(2*pi), 'Color','k')% lambdasColors(n,:))
end

colormap([rgbRainbow(140:end,:);ones(40,1)*[1 0 0]])
hc=colorbar;
cb=540:30:690;
set(hc, 'Ticks',linspace(0,1,6), 'TickLabels',cb,'FontName', 'Times New Roman','Fontsize',6);
hc.Label.String = 'Laser Wavelength [nm]';
set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
xlabel('Voltage [V]','Fontsize',8)
ylabel('Retardance [Radians]','Fontsize',8)
set(gca,'YTick',[0, pi, 2*pi, 3*pi, 4*pi]) 
set(gca,'YTickLabel',{'0','\pi','2\pi','3\pi','4\pi'},'LabelFontsize',8)
title('Comparison of Retardance Curves for multiple Methods','Fontsize',8) 
xlim([0,4])
legend([int,xp],'Interferometric','Crossed Polarizers','Fontsize',6)

print -dpdf LCVRMethodComp.pdf

%% 
mnresidint=[]; mnresidx=[]; maxresidint=[]; maxresidx=[];
count=1;

figure(2)
ax1=axes('Position',[0.13, 0.13, 0.27, 0.7]);
box on
set(gca, 'FontName', 'Times New Roman')
hold on
xlabel('Voltage [V]','Fontsize',8)
ylabel('Residual of Smooth Fitting (Noise) [Radians]','Fontsize',8)
set(gca,'YTick',[-pi/20, -0.1,-0.05,0,0.05,0.10,pi/20]) 
set(gca,'YTickLabel',{'-\pi/20','-0.01','-0.05','0','0.05','0.10','\pi/20'},'LabelFontsize',8)
title({'Residuals of Crossed Polarizers', 'Data across Wavelengths'}) 
ylim([-0.16,0.16])

ax2=axes('Position',[0.55, 0.13, 0.27, 0.7]);
box on
set(gca, 'FontName', 'Times New Roman')
hold on
xlabel('Voltage [V]','Fontsize',8)
ylabel('Residual of Smooth Fitting (Noise) [Radians]','Fontsize',8)
set(gca,'YTick',[-pi/20, -0.10,-0.05,0,0.05,0.10,pi/20]) 
set(gca,'YTickLabel',{'-\pi/20','-0.10','-0.05','0','0.05','0.10','\pi/20'},'LabelFontsize',8)
title({'Residuals of Interferometric','Data across Wavelengths'}) 
ylim([-0.16,0.16])

for i=[550:10:680]
    inxint=find(i==lambdasint);
   %plot(allvoltages(inx,:),radphase0adj(inx,:),'Color','k');%lambdasColorsint(inx,:))

    inx=find(i==lambdas);
    %plot(voltages,rad(inx,:), 'Color', lambdasColors(inx,:));% *lambdas(inx)/(2*pi), 'Color','k')% lambdasColors(n,:))

    [vsint,ixs]=intersect(round(allvoltages(inxint,:),5), [0:0.1:10]);
    valsint=radphase0adj(inxint,ixs);
    [vsx,ixs]=intersect(round(voltages,5), [0:0.1:10]);
    valsx=rad(inx,ixs);
    
    
ft = fittype('a+b/(1+(x/c)^d)^e',...
        'independent',{'x'},'dependent',{'y'},'coefficients',{'a','b','c','d','e'});
    opt = fitoptions(ft);
    opt.Startpoint=[-2,12,2,4.6,0.568]; % rad
    % opt.Startpoint = [-60,1000,1.6,8,0.25]; % nm
    
    [lambdaFit,gof] = fit(vsint',valsint',ft,opt);
    fitRetardance = lambdaFit(vsint)';
    figure(2)
    plot(ax1,vsint,fitRetardance-valsint,'Color',lambdasColorsint(inxint,:))
    mnresidint(count)=mean(abs(fitRetardance-valsint));
    maxresidint(count)=max(abs(fitRetardance-valsint));
    
    [a,b]=rmmissing(valsx);
    [lambdaFit,gof] = fit(vsx(~b)',a',ft,opt);
    fitRetardance = lambdaFit(vsx(~b))';

    plot(ax2,vsx(~b),fitRetardance-a,'Color',lambdasColors(inx,:))
    mnresidx(count)=mean(abs(fitRetardance-a));
    maxresidx(count)=max(abs(fitRetardance-a));

    [commonv,vx,vint]=intersect(round(voltages,5), round(allvoltages(inxint,:),5));
    [diff,b]=rmmissing(rad(inx,vx)-radphase0adj(inxint,vint));
    figure(3)
    hold on
    plot(commonv(~b),diff,'Color',lambdasColors(inx,:))
count=count+1;
end

figure(2)


colormap([rgbRainbow(140:end,:);ones(40,1)*[1 0 0]])
hc=colorbar;
cb=540:30:690;
set(hc, 'Ticks',linspace(0,1,6), 'TickLabels',cb,'FontName', 'Times New Roman','Fontsize',6, 'Position',[0.85, 0.13, 0.03, 0.7]);
hc.Label.String = 'Laser Wavelength [nm]';
set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
%print -dpdf LCVRMethodRes.pdf

mnresidint, maxresidint
mnresidx, maxresidx

[h,p]=ttest2(mnresidint,mnresidx)
mnmnresidint=mean(mnresidint)
mnmnresidx=mean(mnresidx)






