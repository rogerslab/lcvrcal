% Analysis of LCVR Temperature Data
% May 13, 2020
% Nick Schnoor

fname=strcat('20200515_001434_LCVRcal_632.8nm_angles.mat');
% cam_frames
% data
% exposure
% fname
% lambda
% voltages

load(fname);
% theta0=-7; %Mounting offset
% theta=[-5:1:4 5:5:40 41:1:50]; %Angles to test from LCVRAngleCalibration.m
%% Unwrapped Angle retardance curves
l=length(theta);
co=hsv(round(1.4*l));
co=co(1:l,:);
figure(1)
hold on

fftdata = fft(data,size(data,1),1);
Nfringes = find(mean(abs(fftdata(:,:,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
rawphase = squeeze(unwrap(atan2(imag(fftdata(Nfringes+1,:,:)),real(fftdata(Nfringes+1,:,:)))));
phase=rawphase+2*pi;
phase=phase-mean(phase(:,theta==45));
for n=1:l
    plot(voltages, phase(:,n),'Color',co(n,:))
end
colormap(co)
hc=colorbar;
cb=-5:5:50;
set(hc, 'Ticks',linspace(0,1,12), 'TickLabels',cb)%,'FontName', 'Times New Roman','FontSize',8, 'Position',[0.85, 0.13, 0.03, 0.7])
hc.Label.String = 'Angle [Degrees]';
xlabel('Voltage [V]','FontSize',8)
ylabel('Retardance [Radians]','FontSize',8)
yticks([-2*pi, -pi,-0,pi,2*pi]);
yticklabels({'-2\pi','-\pi','0','\pi','2\pi'});
title({'LCVR Fast Axis Orientation Angle';'Effect on Retardance'},'FontSize',10);
%ylim([-inf,850])
xlim([0,10])

set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
%  print -dpdf UnwrappedAngle.pdf

%% cos(2*theta) Fitting of retardance range during rotation

minr=mean(phase(2:11,:),1);
maxr=mean(phase(end-10:end-1,:),1);

r=minr-maxr;

if min(r)<-pi || max(r)>pi
    outofrange=1;
else
    outofrange=0;
end
while outofrange==1
    r(r>pi)=r(r>pi)-pi;
    r(r<-pi)=r(r<-pi)+pi;
    if min(r)<-pi || max(r)>pi
        outofrange=1;
    else
        outofrange=0;
    end
end

figure(2)
d=plot(theta,r,'k-*');
yticks([-pi, -pi/2, 0, pi/2, pi]);
set(gca,'yticklabel',{'-\pi','-\pi/2','0','\pi/2','\pi'},'FontName', 'Times New Roman','FontSize',8);
%xticklabels('FontName', 'Times New Roman','FontSize',8)


xlabel('Rotation about Z-axis [Degrees]','FontName', 'Times New Roman','FontSize',10)
ylabel('Max-Min Retardation [radians]','FontName', 'Times New Roman','FontSize',10)
title({'Effect of Fast Axis Orientation'; ['LCVR Range of Retardation, ', num2str(lambda),' nm']},'FontName', 'Times New Roman','FontSize',12)
ylim([-pi,pi])

hold on

ft=fittype('a+b.*cosd(2*(x+c))',...
    'independent',{'x'},'dependent',{'y'},'coefficients',{'a','b','c'});
opt=fitoptions(ft);
opt.Startpoint=[0, pi, 0];
[nmfit,gof]=fit(theta',r',ft,opt);
y2=nmfit(-10:0.5:55);
f=plot(-10:0.5:55, y2,'r-','LineWidth',1.0);
a=['a = ',num2str(nmfit.a),' rad'];
b=['b = ',num2str(nmfit.b),' rad'];
c=['c = ',num2str(nmfit.c),' deg'];
leg=legend([d,f],{'Data',[' ', newline, 'Fit a+b*cos(2(x+c))', newline, a , newline,  b, newline, c]});
disp(['Estimated theta0=',num2str(2*nmfit.c)]);

set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
%print -dpdf AngleFit.pdf

%% Wrapped and Unwrapped Retardance Curves
figure(3)

subplot(1,2,1)
hold on
while sum(phase>pi,'all')+sum(phase<-pi,'all')~=0
    phase=phase-mean(phase(:,theta==45));
    phase=phase+((phase<-pi)-(phase>pi))*2*pi;
end

for n=1:l
    plot(voltages, phase(:,n),'Color',co(n,:))
end
colormap(co)
hc=colorbar;
cb=-5:5:50;
set(hc, 'Ticks',linspace(0,1,12), 'TickLabels',cb,'Position',[0.85, 0.13, 0.03, 0.7])%,'FontName', 'Times New Roman','FontSize',8, 'Position',[0.85, 0.13, 0.03, 0.7])
hc.Label.String = 'Angle [Degrees]';
xlabel('Voltage [V]','FontSize',8)
ylabel('Retardance [Radians]','FontSize',8)
yticks([-2*pi, -pi,-0,pi,2*pi]);
yticklabels({'-2\pi','-\pi','0','\pi','2\pi'});
sgtitle({'LCVR Fast Axis Orientation Angle';'Effect on Retardance'},'FontSize',10);
title('Wrapped','FontSize',8)
ylim([-2.5*pi,2.5*pi])
xlim([0,10])
set(gca,'Position',[0.1, 0.12, 0.26,0.7])

s=std(phase,0,2);
[~,imax]=max(s);
[~,imin1]=min(s(1:imax));
[~,imin2]=min(s(imax:end));

keyvoltages=[voltages(imax), voltages(imin1), voltages(imin2+imax-1)]


subplot(1,2,2)
hold on
for n=1:l
    if rawphase(1,n)> (rawphase(end,n)+pi)
        adjphase(:,n)=2*pi-rawphase(33,n)+rawphase(:,n);
    elseif rawphase(1,n)<(rawphase(end,n)-pi)
        adjphase(:,n)=-2*pi-rawphase(33,n)+rawphase(:,n);
    else
        adjphase(:,n)=rawphase(:,n)-mean(rawphase(:,n));
    end
    plot(voltages, adjphase(:,n),'Color',co(n,:))
end

xlabel('Voltage [V]','FontSize',8)
ylabel('Retardance [Radians]','FontSize',8)
yticks([-3*pi, -2*pi, -pi,-0,pi,2*pi, 3*pi]);
yticklabels({'-3\pi','-2\pi','-\pi','0','\pi','2\pi','3\pi'});
title('Unwrapped','FontSize',8)%ylim([-inf,850])
xlim([0,10])
ylim([-2.5*pi,2.5*pi])
set(gca,'Position',[0.5, 0.12, 0.26, 0.7])
set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
%print -dpdf CombinedAngle2.pdf
