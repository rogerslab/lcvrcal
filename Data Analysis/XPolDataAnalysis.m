%I=I_0*sin^2(2*theta)*cos^2(delta phi/2)
    load('LCVRxpoldataSUPERK_alltwo.mat', 'voltages', 'intensities', 'lambdas')
intensities(6,1)=NaN;

rgbRainbow = hsv(300);
rgbRainbow = flipud(rgbRainbow(1:251,:));
lambdasColors=zeros(length(lambdas),3); 

v=find(voltages==2.3);    
[max1, max1ix]=max(intensities(:,1:v),[],2);
[min1, min1ix]=min(intensities(:,1:v),[],2);    
[max2, max2ix]=max(intensities(:,v:end),[],2);    
[min2,min2ix]=min(intensities(:,v:end),[],2);
max2ix=max2ix+v-1;
min2ix=min2ix+v-1;

for n=1:length(lambdas)
    if n<7
        normIntensities(n,:)=[(intensities(n,1:min1ix(n))-min1(n))./(max1(n)-min1(n)),...
    (intensities(n,min1ix(n)+1:max2ix(n))-min1(n))./(max2(n)-min1(n)),...
    (intensities(n,max2ix(n)+1:end)-min2(n))./(max2(n)-min2(n))];
radwrapped(n,:)=real(2*acos(sqrt(normIntensities(n,:))));
rad(n,:)=[radwrapped(n,1:max1ix(n))+2*pi,-radwrapped(n,max1ix(n)+1:min1ix(n))+2*pi,...
    radwrapped(n,min1ix(n)+1:max2ix(n)),-radwrapped(n,max2ix(n)+1:min2ix(n)),...
    radwrapped(n,min2ix(n)+1:end)-2*pi]+pi;
    else
            normIntensities(n,:)=[(intensities(n,1:max2ix(n))-min1(n))./(max2(n)-min1(n)),...
    (intensities(n,max2ix(n)+1:end)-min2(n))./(max2(n)-min2(n))];
radwrapped(n,:)=real(2*acos(sqrt(normIntensities(n,:))));
rad(n,:)=[-radwrapped(n,1:min1ix(n))+2*pi, radwrapped(n,min1ix(n)+1:max2ix(n)),...
    -radwrapped(n,max2ix(n)+1:min2ix(n)), radwrapped(n,min2ix(n)+1:end)-2*pi]+pi;
    end
    if lambdas(n) <= 400
        lambdasColors(n,:) = rgbRainbow(1,:);
    elseif lambdas(n) >= 650
        lambdasColors(n,:) = rgbRainbow(251,:);
    else
        lambdasColors(n,:) = rgbRainbow(round(lambdas(n))-399,:);
    end
end

nm=lambdas*rad/(2*pi);

for n=1:length(lambdas)

figure(1)
hold on
plot(voltages,normIntensities(n,:),'Color',lambdasColors(n,:))

figure(2)
hold on
plot(voltages,radwrapped(n,:),'Color',lambdasColors(n,:))

figure(3)
hold on
plot(voltages,rad(n,:),'Color',lambdasColors(n,:))

figure(4)
hold on
plot(voltages,nm(n,:)*lambdas(n)/(2*pi), 'Color', lambdasColors(n,:))
end
    
    
    
 