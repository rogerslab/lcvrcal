% Analysis of LCVR Wavelength Data
% May 13, 2020
% Nick Schnoor


%Wavelength Data file variables:
% data
% exposure
% lambda
% measuredtemps
% measuredtempspost
% measuredtempspre
% tempsetpts =25
% voltages:          length 273

wavelengths=500:10:680;
theta=[0, 45];
figure(1)
hold on
for i=wavelengths
    if i<540
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths.mat'])
        fftdata = fft(data,size(data,1),1);         
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));
        
        radphase0(wavelengths==i,:)=phase(:,1,theta==0);
        radphase45(wavelengths==i,:)=phase(:,1,theta==45);
        
        lambdas(wavelengths==i)=lambda;
        allvoltages(wavelengths==i,:)=voltages;
        
    elseif i==540
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths.mat'])
        fftdata = fft(data,size(data,1),1);
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));

        radphase0(wavelengths==i,:)=phase(:,1,theta==0);
        radphase45(wavelengths==i,:)=phase(:,1,theta==45);
        
        lambdas(wavelengths==i)=lambda;
        allvoltages(wavelengths==i,:)=voltages;
        
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths (2).mat'])
        fftdata = fft(data,size(data,1),1);
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));

        radphase0(find(wavelengths==i)+1,:)=phase(:,1,theta==0);
        radphase45(find(wavelengths==i)+1,:)=phase(:,1,theta==45);
        
        lambdas(find(wavelengths==i)+1)=lambda;
        allvoltages(find(wavelengths==i)+1,:)=voltages;
        
     elseif i>540
         
        load(['wavelength\LCVRcal_',num2str(i),'nm_wavelengths.mat'])
        fftdata = fft(data,size(data,1),1);
        Nfringes = find(mean(abs(fftdata(:,:,1,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
        phase = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,:,:)),real(fftdata(Nfringes+1,:,:,:)))),size(data,2),size(data,3),size(data,4));

        radphase0(find(wavelengths==i)+1,:)=phase(:,1,theta==0);
        radphase45(find(wavelengths==i)+1,:)=phase(:,1,theta==45);
        
        lambdas(find(wavelengths==i)+1)=lambda;
        allvoltages(find(wavelengths==i)+1,:)=voltages;
    end
end
nmphase0=lambdas'*ones(1,size(allvoltages,2)).*radphase0/(2*pi);
nmphase45=lambdas'*ones(1,size(allvoltages,2)).*radphase45/(2*pi);

%% 
cutoff=550;
exclude=[560];
wavelengthindex=[1:length(lambdas)];
[~,i]=intersect(lambdas,exclude);
wavelengthindex(i)=[];
wavelengthindex(lambdas<cutoff)=[];

num=length(wavelengthindex);

c=hsv(54);
c=flipud(c(1:19,:));
figure(1)
hold on
for i=wavelengthindex
    plot(allvoltages(i,:),nmphase0(i,:),'Color',c(lambdas(i)==wavelengths,:))
end
colormap(c)
hc=colorbar;
cb=500:30:680;
set(hc, 'Ticks',linspace(0,1,7), 'TickLabels',cb)%,'FontName', 'Times New Roman','FontSize',8, 'Position',[0.85, 0.13, 0.03, 0.7])
hc.Label.String = 'Wavelength [nm]';

figure(2)
hold on
for i=wavelengthindex
    plot(allvoltages(i,:),nmphase45(i,:),'Color',c(lambdas(i)==wavelengths,:))
end
colormap(c)
hc=colorbar;
cb=500:30:680;
set(hc, 'Ticks',linspace(0,1,7), 'TickLabels',cb)%,'FontName', 'Times New Roman','FontSize',8, 'Position',[0.85, 0.13, 0.03, 0.7])
hc.Label.String = 'Wavelength [nm]';

%% 
d=zeros(20,133-41);

for t=41:133
    d(:,t-40)=(radphase45(:,t+1)-radphase45(:,t))./(allvoltages(:,t+1)-allvoltages(:,t));
end
    [v,ix]=max(abs(d),[],2);
    ix=ix+40;

c=hsv(54);
c=flipud(c(1:19,:));

filename = 'LC# G14087.txt';
fileID = fopen(filename);
    
% Reads file data into an array. Reads in 3 floating point numbers
% and skips 7 headerlines.
    MLcaldata = textscan(fileID,'%f%f%f','headerlines',7);
    MLcaldataV=cell2mat(MLcaldata(1))/1000; % voltage
    MLcaldatanm = cell2mat(MLcaldata(2)); % retardance
    MLcaldatawaves=cell2mat(MLcaldata(3));
    

figure(3)
sgtitle('Spectral Dependence of LCVR Retardance Curve','FontSize',10,'FontName','Times New Roman');

ax1=axes('Position',[0.13, 0.13, 0.27, 0.7]);
box on
set(gca, 'FontName', 'Times New Roman')
hold on

for i=wavelengthindex
    radphase0adj(i,:)=radphase0(i,:)+(pi-radphase0(i,ix(i)));
    plot(ax1,allvoltages(i,:),radphase0adj(i,:),'Color',c(lambdas(i)==wavelengths,:))
end
plot(MLcaldataV(1:170),MLcaldatawaves(1:170)*2*pi,'Color','k','Linewidth',1.0);
% legend(a,'Meadowlark Supplied Data (633 nm)')

xlabel('Voltage [V]','Fontsize',8)
ylabel('Retardance of different Wavelengths [Radians]','Fontsize',8)
set(gca,'YTick',-pi:pi:4*pi) 
set(gca,'YTickLabel',{'-\pi','0','\pi','2\pi','3\pi','4\pi'},'LabelFontsize',8)
hold off


ax2=axes('Position',[0.55, 0.13, 0.27, 0.7]);
box on
set(gca, 'FontName', 'Times New Roman')
hold on
for i=wavelengthindex
    nmphase0adj(i,:)=nmphase0(i,:)+(lambdas(i)/2-nmphase0(i,ix(i)));
    plot(ax2,allvoltages(i,:),nmphase0adj(i,:),'Color',c(lambdas(i)==wavelengths,:))
end
a=plot(MLcaldataV(1:170),MLcaldatanm(1:170),'Color','k','Linewidth',1.0);
legend(a,['Meadowlark Data (633 nm)'], 'Position',[0.33,0.86,0.3,0.05])


xlabel('Voltage [V]','Fontsize',8)
ylabel(['Retardance of different Wavelengths [nm]'],'Fontsize',8)
hold off

colormap(c)
hc=colorbar;
cb=500:30:680;
set(hc, 'Ticks',linspace(0,1,7), 'TickLabels',cb,'FontName', 'Times New Roman','Fontsize',6, 'Position',[0.85, 0.13, 0.03, 0.7]);
hc.Label.String = 'Laser Wavelength [nm]';
set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
% print -dpdf LCVRWavelengthRetake.pdf

% 
% 
% for i=1:20
%     radphase0adj(i,:)=radphase0(i,:)+(pi-radphase0(i,ix(i)));
%     
%     figure(6)
%     hold on
%     plot(allvoltages(i,:),radphase0adj(i,:),'Color',c(lambdas(i)==wavelengths,:))
% end
% colormap(c)
% hc=colorbar;
% cb=500:30:680;
% set(hc, 'Ticks',linspace(0,1,7), 'TickLabels',cb)%,'FontName', 'Times New Roman','FontSize',8, 'Position',[0.85, 0.13, 0.03, 0.7])
% hc.Label.String = 'Wavelength [nm]';
%% 
   
l=lambdas'*ones(1,size(allvoltages,2));
figure(4)
hold on
for i=wavelengthindex
    plot3(allvoltages(i,:),l(i,:),nmphase0adj(i,:),'Color',c(lambdas(i)==wavelengths,:),'Marker','o')
end
%% 
% figure(4)
% hold on
for i=wavelengthindex
    ft=fittype('a+b/(1+(x/c)^d)^e',...
    'independent',{'x'},'dependent',{'y'},'coefficients',{'a','b','c','d','e'});
    opt=fitoptions(ft);
    %opt.Startpoint=[-2,12,2,4.6,0.568]; %rad
    opt.Startpoint=[-60,1000,1.6,8,0.25]; %nm
    
    [wlfit,gof2]=fit(allvoltages(i,:)',nmphase0adj(i,:)',ft,opt); % ,'problem','LAS');
    y2=wlfit(allvoltages(i,:))';
    plot3(allvoltages(i,:), l(i,:),y2,'k','LineWidth',0.5)
    coeffs(i,:)=coeffvalues(wlfit)';
    q=cell2mat(struct2cell(gof2));
    r(i,:)=q(2);
    residuals(i,:)=y2-nmphase0adj(i,:);
end

r=nonzeros(r);
coeffs=reshape(nonzeros(coeffs),num,5);
residuals=reshape(nonzeros(residuals),num,size(nmphase0adj,2));
%% 
ft2=fittype({'1','x','x^2'});
fc=zeros(5,3);

for i=1:5
    [cofit,gof]=fit(lambdas(wavelengthindex)',coeffs(:,i),ft2);
    figure(i+6)
    hold on
    plot(lambdas(wavelengthindex),coeffs(:,i))
    y5=cofit(485:675);
    plot(485:675, y5);
    fc(i,:)=coeffvalues(cofit);
end


%% SURFACE PLOT OF APPROXIMATION
vs=0:0.1:10;
wls=525:5:700;
[x1,y1]=meshgrid(vs,wls);


%c=zeros(length(wls),1);
%for m=2
b=fc(2,1)+fc(2,2).*wls+fc(2,3).*wls.^2; 
%end
meancoeffs=mean(coeffs,1);
disp(['a= ',num2str(meancoeffs(1))])
disp(['b1= ', num2str(fc(2,1)), '   b2= ', num2str(fc(2,2)), '   b3= ', num2str(fc(2,3))])
disp(['c= ', num2str(meancoeffs(3))])
disp(['d= ', num2str(meancoeffs(4))])
disp(['e= ', num2str(meancoeffs(5))])

z=meancoeffs(1) + b'./(1+(vs./meancoeffs(3)).^meancoeffs(4)).^meancoeffs(5);
% z=c(:,1).*x1.^7 + c(:,2).*x1.^6 + c(:,3).*x1.^5 + c(:,4).*x1.^4 + c(:,5).*x1.^3 + c(:,6).*x1.^2 + c(:,7).*x1 + c(:,8);
figure(4)
set(gca, 'FontName', 'Times New Roman')
hold on

surf(vs,wls,z)
colormap(bone)
xlabel('Voltage [V]','FontSize',8)
ylabel('Wavelength [nm]','FontSize',8)
zlabel('Retardance [nm]','FontSize',8)

%shading interp
title('Surface Fit of Wavelength Dependence of Retardance ','FontSize',10)
set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
view(60,15)
%print -dpdf LCVRwlfit.pdf

% 
% phi=[1,0.75,0.5,0.25];
% 
% vactual=zeros(20,length(phi));
% vtheory=vactual;
% wactual=vactual;
% wtheory=ones(20,1)*phi;
% for n=1:20
%     vactual(n,:)=interp1(wlphaseall(n,2:42),x3,phi);
%     l=lambdas(n);
%     ix=(wls==l)*z;
%     vtheory(n,:)=interp1(ix,vs,phi); 
%     wactual(n,:)=interp1(vs,ix,vactual(n,:));
% end
% verrors=abs(vtheory-vactual)
% werrors=abs(wactual-wtheory)
% mean(werrors)

%% 

bres=fc(2,1)+fc(2,2).*lambdas(wavelengthindex)+fc(2,3).*lambdas(wavelengthindex).^2;
vals=meancoeffs(1) + bres'./(1+(allvoltages(wavelengthindex,:)./meancoeffs(3)).^meancoeffs(4)).^meancoeffs(5);

surfresiduals=nmphase0adj(wavelengthindex,:)-vals;
mean(abs(surfresiduals),'all')
max(abs(surfresiduals),[],'all')
%% 633 fit
mlwl=633;
b=fc(2,1)+fc(2,2)*mlwl+fc(2,3)*mlwl^2;
mlfit=meancoeffs(1) + b./(1+(MLcaldataV(1:170)./meancoeffs(3)).^meancoeffs(4)).^meancoeffs(5);

figure(5)
hold on
yyaxis left
a=plot(MLcaldataV(1:170),MLcaldatanm(1:170),'k.','Linewidth',1.0);
d=plot(MLcaldataV(1:170),mlfit,'Color','r');
legend('Meadowlark Data (633 nm)','633 nm Fit Interpolation')

xlabel('Voltage [V]','Fontsize',8)
ylabel('Retardance [nm]','Fontsize',8)

mean(abs(MLcaldatanm(1:170)-mlfit))
max(abs(MLcaldatanm(1:170)-mlfit))

ybar=1/170*sum(MLcaldatanm(1:170));
sstot=sum((MLcaldatanm(1:170)-ybar).^2);
ssres=sum((MLcaldatanm(1:170)-mlfit).^2);
rsquared=1-ssres/sstot;
yyaxis right
plot(MLcaldataV(1:170),MLcaldatanm(1:170)-mlfit,'Color','g')

legend('Meadowlark Data (633 nm)',['633 nm Fit Interpolation R^2 = ',num2str(rsquared)],'Residuals');

%% 633 self fit
figure(6)
hold on
yyaxis left
a=plot(MLcaldataV(1:170),MLcaldatanm(1:170),'k.','Linewidth',1.0);

ft=fittype('a+b/(1+(x/c)^d)^e',...
    'independent',{'x'},'dependent',{'y'},'coefficients',{'a','b','c','d','e'});
opt=fitoptions(ft);
opt.Startpoint=[-60,1000,1.6,8,0.25]; %nm

[wlfit,gof2]=fit(MLcaldataV(1:170),MLcaldatanm(1:170),ft,opt)
mlfit=wlfit(MLcaldataV(1:170));
plot(MLcaldataV(1:170),mlfit,'r')%,'LineWidth',0.5)
mlcoeffs(i,:)=coeffvalues(wlfit)';
q=cell2mat(struct2cell(gof2));
mlr=q(2);
MLresiduals=MLcaldatanm(1:170)-mlfit;

xlabel('Voltage [V]','Fontsize',8)
ylabel('Retardance [nm]','Fontsize',8)
set(gca,'YColor','r');

yyaxis right
plot(MLcaldataV(1:170),MLcaldatanm(1:170)-mlfit,'Color','g')
ylabel('Fit Residual [nm]','Fontsize',8)
legend('Meadowlark Data (633 nm)',['633 nm Fit Interpolation R^2 = ',num2str(mlr)],'Residuals','Fontsize',5,'Color','none','EdgeColor','none','Location','south')%,'Position',[0.33,0.86,0.3,0.05]);
title('Manufacturer Retardance Data Fit')
set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])

set(gca,'YColor','g');

%print -dpdf LCVRmlfit.pdf















