% Analysis of LCVR Temperature Data
% May 13, 2020
% Nick Schnoor

fname=strcat('20200508_151314_LCVRcal_632.8nm_temps.mat');
% cam_frames
% data
% exposure
% fname
% lambda
% measuredtemps:     length 57  = avg of post and pre
% measuredtempspost: length 57
% measuredtempspre:  length 57
% tempsetpts:        length 56
% voltages:          length 201

%Took data for ambient temp + every tempsetpts: 1+56=57

load(fname);

%%
l=length(tempsetpts)+1;
c=hsv(round(1.4*l));
c=c(1:l+5,:);
figure(1)
hold on

fftdata = fft(data,size(data,1),1);
Nfringes = find(mean(abs(fftdata(:,:,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
phase = squeeze(unwrap(atan2(imag(fftdata(Nfringes+1,:,:)),real(fftdata(Nfringes+1,:,:)))));
phase=phase+2*pi;
for n=1:l
 plot(voltages*1000, phase(:,n),'Color',c(n+4,:)) 
end
colormap(c)
hc=colorbar;
cb=20:5:50;
set(hc, 'Ticks',linspace(0,1,7), 'TickLabels',cb)%,'FontName', 'Times New Roman','FontSize',8, 'Position',[0.85, 0.13, 0.03, 0.7])
hc.Label.String = 'Temperature [degrees C]';
xlabel('Voltage [mV]','FontSize',8)
ylabel(['Phase Difference Between Horizontal' newline 'and Vertical Polarized Light [nm]'],'FontSize',8)
yticks([-4*pi,-3*pi,-2*pi,-pi,0]);
yticklabels({'-4\pi','-3\pi','-2\pi','-\pi','0'});
%title({'';'LCVR Temperature Effect on Phase Retardance';'Comparison to Meadowlark Data'},'FontSize',10);
%ylim([-inf,850])
xlim([0,10000])



minr=mean(phase(2:11,:),1);
maxr=mean(phase(end-10:end-1,:),1);

figure(2)
%a=plot(measuredtemps,minr-maxr,'b','LineWidth',1.3)
yticks([2.3*pi, 2.4*pi,2.5*pi,2.6*pi,2.7*pi]);
set(gca,'yticklabel',{'2.3\pi','2.4\pi','2.5\pi','2.6\pi','2.7\pi'},'FontName', 'Times New Roman','FontSize',8);
%xticklabels('FontName', 'Times New Roman','FontSize',8)
hold on
lr=fitlm(measuredtemps,minr-maxr);
b=plot(lr)
b(1).Color=[0,0,0];
b(1).Marker='o';
b(1).MarkerFaceColor=[0, 0, 0];
b(1).MarkerSize=4.0;
b(2).Color=[0,0,1];
b(2).LineWidth=1.3;
b(3).LineWidth=1.2;
b(4).LineWidth=1.2;
xlabel(['Temperature [', char(176),'C]'],'FontName', 'Times New Roman','FontSize',10)
ylabel('Max-Min Retardance [radians]','FontName', 'Times New Roman','FontSize',10)
title({'Effect of Temperature on'; ['LCVR Range of Retardance, ', num2str(lambda),' nm']},'FontName', 'Times New Roman','FontSize',12)

slope=table2array(lr.Coefficients(2,1));
ci=coefCI(lr);

legend([b(1),b(2),b(3)],{'Data',['Fit, Slope = [', num2str(round(slope,4)), char(177),num2str(round((ci(2,2)-ci(2,1))/2,3)),'] rad/', char(176),'C'], '95% Fit Confidence Region'})


set(gcf,'PaperSize',[4, 3],'PaperPosition',[0, 0, 4, 3])
print -dpdf Temperature.pdf

%"retardance decreases by approximately 0.2% to 0.3% per �C increase in temperature"


%% Temperatures With Zero

fname=strcat('20200515_090415_LCVRcal_632.8nm_temperatureswith0.mat');
load(fname)


fftdata = fft(data,size(data,1),1);
Nfringes = find(mean(abs(fftdata(:,:,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
phase = squeeze(unwrap(atan2(imag(fftdata(Nfringes+1,:,:)),real(fftdata(Nfringes+1,:,:)))));

t=[measuredtemps, measuredtemps];
figure(3)
hold on
for n=1:size(phase,2)
 plot(voltages*1000, phase(:,n),'Color','k') 
end














