*****Make sure LCVR Drivers/Documentation Folder is added to path in MATLAB*****


LCVR Commands in MATLAB:

LCVRcmd('inv:n,mv')			Sends Voltage to an LCVR
	inputs
	n	channel (1 or 2)
	mv	voltage (in mv) to be applied to LCVR n (0-10000)
	outputs
	returns 'inv:' -- useless and annoying 



LCVRcmd('tsp:n,t')			Sets Temperature for an LCVR
	inputs
	n	channel (1 or 2)
	t	temperature (16-bit)*
	outputs
	returns 'tsp:' -- annoying and useless



LCVRcmd('tmp:n,?)			Queries LCVR for Temperature
	inputs
	n	channel (1 or 2)
	outputs
	returns 'tmp:n,t' -- can extract t**



Note:
LCVR uses active heating, but passive cooling so can take a long time to cool. Heating takes time too, so set temp, then keep querying  in a loop until query gets close (within 0.1C or so).



*	To convert from a temp in Celsius (tempC) to these units (t), use MATLAB
	code:

	t = round((tempC + 273.15) * 65535/500;
	LCVRcmd(['tsp:n,',num2str(t)]);




**	To get temp as an integer in degrees C (tempC), use MATLAB code:

	t = LCVRcmd('tmp:n,?');
	tempC = str2double(t(7:12) * 500/65535 - 273.15;