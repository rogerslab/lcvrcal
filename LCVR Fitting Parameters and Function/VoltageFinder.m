% Function for finding a Voltage for a desired retardance and wavelength
% when using an LCVR. 
% Inputs:
%    paramFile         'G14085Params.mat', 'G14087Params.mat'
%    desiredRetardance in radians i.e. pi/2, 2*pi, etc.
%    wavelength        in nm
% Outputs:
%    V                 in volts

function V=VoltageFinder(paramFile,desiredRetardance,wavelength)
load(paramFile);
nmR=desiredRetardance/(2*pi)*wavelength;
a=a1+a2/wavelength.^2;
b=b1+b2/wavelength.^2;
V=c*((b'/(nmR-a')).^(1/e)-1).^(1/d);
end