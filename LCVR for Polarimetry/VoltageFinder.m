% Function for finding a Voltage for a desired retardance and wavelength
% when using an LCVR. 
% Inputs:
%    LCVR         14085 or 14087
%    desiredRetardance in radians i.e. pi/2, 2*pi, etc.
%    wavelength        in nm
% Outputs:
%    mV                 in millivolts

function mV=VoltageFinder(LCVR,desiredRetardance,wavelength)
retardances=[0:pi/2:2*pi];
LCVRs=[14085, 14087];
if LCVR ~=LCVRs
    disp('Unknown LCVR unit number. Only 14085 and 14087 are known.')
elseif desiredRetardance ~=retardances
    disp('Uncalibrated Retardance. Suitable retardances are 0:pi/2:2*pi')
elseif wavelength<500 || wavelength>700
    disp('Wavelength out of range. Must be between 500-700 nm')
else
    i=find(LCVR==LCVRs);
    j=find(desiredRetardance==retardances);
    load('LCVRparams.mat','params');
    mV=round(params(i,j,1)*wavelength^2+params(i,j,2)*wavelength+params(i,j,3));
end
