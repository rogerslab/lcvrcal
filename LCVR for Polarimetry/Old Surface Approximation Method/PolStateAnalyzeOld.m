% Function for Filtering Pol states with two LCVRs. Will send proper
% voltage to LCVRs automatically
%
% Inputs:
%   StokeVector: 4x1 vector for the pure pol state to be analyzed
%   wavelength: wavelength of the laser
%
% Outputs:
%   none

% PS Analyzer (2 LCVRs numbered by position along optical axis
%               theta=rotation, delta=retardance):

%[LCVR 2 MM] * [LCVR 1 MM] = [Mueller Matrix]

% In: Horiz/Vert  --->   Out: Horiz/Vert
% [1 0 0 0]   [  1 ]   [  1 ]       theta1 = 0
% [0 1 0 0] * [+/-1] = [+/-1]       delta1 = 0
% [0 0 1 0]   [  0 ]   [  0 ]       theta2 = pi/4
% [0 0 0 1]   [  0 ]   [  0 ]       delta2 = 0

% In: RHC/LHC     --->   Out: Horiz/Vert
% [1  0 0 0]   [  1 ]   [  1 ]       theta1 = 0
% [0  0 0 1] * [  0 ] = [+/-1]       delta1 = 0
% [0  0 1 0]   [  0 ]   [  0 ]       theta2 = pi/4
% [0 -1 0 0]   [+/-1]   [  0 ]       delta2 = pi/2

% In: +45/-45     --->   Out: Horiz/Vert
% [1  0 0  0]   [  1 ]   [  1 ]       theta1 = 0
% [0  0 1  0] * [  0 ] = [+/-1]       delta1 = pi/2
% [0  0 0 -1]   [+/-1]   [  0 ]       theta2 = pi/4
% [0 -1 0  0]   [  0 ]   [  0 ]       delta2 = pi/2

function PolStateAnalyze(StokeVector,wavelength)
lcvr1='G14085';
lcvr2='G14087';
if StokeVector==[1;1;0;0] | StokeVector==[1;-1;0;0]
    delta1=0;
    delta2=0;
elseif StokeVector==[1;0;1;0] | StokeVector==[1;0;-1;0]
    delta1=pi/2;
    delta2=pi/2;
elseif StokeVector==[1;0;0;1] | StokeVector==[1;0;0;-1]
    delta1=0;
    delta2=pi/2;
else
    disp('Invalid Stoke Vector')
end
mV1=VoltageFinder([lcvr1,'Params.mat'],delta1,wavelength);
mV2=VoltageFinder([lcvr2,'Params.mat'],delta2,wavelength);
LCVRcmd(['inv:1,',num2str(mV1)])
LCVRcmd(['inv:2,',num2str(mV2)])
disp('LCVRs Set')
end
