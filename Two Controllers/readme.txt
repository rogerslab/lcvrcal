LCVRcal/Two_Controllers includes the mex file and driver files to run two D5020 LCVR controllers connected bia USB to the same computer.

To use in MatLab, use the function
    out=LCVRcmd2controllers(devnum, cmd);

    Inputs:
        devnum: Integer. Device number of the controller you want to use (ie 1 or 2)*
        cmd:    String. Command to be sent to the controller (ie 'inv:1,10000'). 
                See Table 2 in the D5020 Manual for a full rundown of usable command strings.

    Outputs:
        out:    Char String. Only applicable/useful for querying commands with '?'  


*Only difference from LCVRcmd() function is the extra devnum input to reference the specific controller