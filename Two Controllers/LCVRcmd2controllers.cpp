/* LCVRcmd2.cpp  
 * Meadowlark Optics D5020 LCVR controller
 * written by Aaron Woods <alwoods2@miners.utep.edu>
 * July 16, 2018
 *
 * Based heavily on example cpp code from MLO, and code from J.D. Rogers
 *
 * usage:
 * This program allows the ability to send ASCII Firmware Commands to multiple
 * connected Digital Interface Controllers as an integer for device number and a
 * string for command in the matlab function argument.
 *
 * out = LCVRcmd2controllers(devnum, cmd);
 *
 * Inputs:
 * 	devnum: an inteer that indicates which D5020 controller is being called
 *	cmd:	a string with the LCVR command
 *
 * Outputs:
 * out = status output of command. Only useful for some commands
 *
 *
 * Here is a list of the ASCII string commands for the digital interface controller.
 * See Table 2 of the D5020 Digital Controller User Manual for more info.
 *  inv:n,v1                        tmp:n,?
 *  sin:n,v1,v2,t,ph                trg:n,?
 *  saw:n,v1,v2,t,ph                tsp:n,t
 *  tri:n,v1,v2,t,ph                tsp:n,?
 *  sqr:n,v1,v2,t,ph,dc             ver:?
 *  tnew:n,v1,v2,t,ph,dc,tv,tt      thr:n,v1,v2
 *  sync:n,ph,t                     ext:n
 */

#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <iostream>
#include <windows.h>
#include "USBDRVD.h"
#include <string>

void mexFunction( int nlhs, mxArray *plhs[],        /* Left Hand Sife of Function */
                  int nrhs, const mxArray *prhs[])   /* Right Hand Sife of Function */
{   
    
/********************************************************************/      
class mystream : public std::streambuf
{
protected:
virtual std::streamsize xsputn(const char *s, std::streamsize n) { mexPrintf("%.*s", n, s); return n; }
virtual int overflow(int c=EOF) { if (c != EOF) { mexPrintf("%.1s", &c); } return 1; }
};
class scoped_redirect_cout
{
public:
	scoped_redirect_cout() { old_buf = std::cout.rdbuf(); std::cout.rdbuf(&mout); }
	~scoped_redirect_cout() { std::cout.rdbuf(old_buf); }
private:
	mystream mout;
	std::streambuf *old_buf;
};
scoped_redirect_cout mycout_redirect;
//static scoped_redirect_cout mycout_redirect;

/****************************************************************/   
/* Check arguments                                              */

    if(nrhs != 2)
    {mexErrMsgIdAndTxt("bitmarker:LCVRcmd2", "Two inputs required.");}

    if(!mxIsDouble(prhs[0]))
	{mexErrMsgIdAndTxt("bitmarker:LCVRcmd2", "Argument 1 must be inputed as an integer.");}
if(!mxIsChar(prhs[1])) 
    {mexErrMsgIdAndTxt("bitmarker:LCVRcmd2", "Argument 2 must be inputed as a character.");}
    
/************************************************************************
*
*                               Meadowlark Optics
*                               Copyright (2006-2017)
*
* File name: usb_ver.cpp
*
* Description: This file contains sample source code that will initialize
*              a USB connection to a Meadowlark Optics D5020, send a
*              ver:? command and read the status response.
*
************************************************************************/

// void main()
// {
#define  flagsandattrs  0x40000000
/***********************************************************************/
// Converts string to a unsigned char array

// Indicates which controller to be referenced
std::string devnumstr=mxArrayToString(prhs[0]);
int devnum=stoi(devnumstr);
// Assigns the MATLAB input argument to string "str"
std::string str = mxArrayToString(prhs[1]);

// Concatenation with the += operator
str += '\n';  

// Assigns string length
int slength = str.length();

// Declaring Character Array
char* cmd2 = new char[slength+1];   

// Copying the contents of the string to the char array
strcpy(cmd2,str.c_str());   

// Casts signed character array to unsigned character array
unsigned char *cmd3 = (unsigned char *)cmd2;
/***********************************************************************/
     
//unsigned char cmd[] = {'v','e','r',':','?','\n'};
//std::string cmd = {"ver:?\n"}; //Strings don't work here it seems
unsigned char status [64];

INT cmd_size = slength; //The size of the byte array of the command.
        
HANDLE dev1, pipe0, pipe1;

UINT devcnt, i;
UINT USB_PID;

GUID  theGUID;

theGUID.Data1 = 0xa22b5b8b;
theGUID.Data2 = 0xc670;
theGUID.Data3 = 0x4198;
theGUID.Data4 [0] = 0x93;
theGUID.Data4 [1] = 0x85;
theGUID.Data4 [2] = 0xaa;
theGUID.Data4 [3] = 0xba;
theGUID.Data4 [4] = 0x9d;
theGUID.Data4 [5] = 0xfc;
theGUID.Data4 [6] = 0x7d;
theGUID.Data4 [7] = 0x2b;

USB_PID = 0x139C; //PID for the first D5020.

devcnt=USBDRVD_GetDevCount(USB_PID);

if (devcnt == 0)
   {
	std::cout<<"No Meadowlark Optics USB Devices Present."<<std::endl;
   }
else
	{
	/* open device and pipes */
	dev1 =USBDRVD_OpenDevice(devnum, flagsandattrs, USB_PID);
	pipe0 =USBDRVD_PipeOpen(devnum, 0, flagsandattrs, &theGUID);
	pipe1 =USBDRVD_PipeOpen(devnum, 1, flagsandattrs, &theGUID);
	/* send command */
	USBDRVD_BulkWrite (dev1, 1, cmd3, cmd_size);

   /* read status response */
	USBDRVD_BulkRead (dev1, 0, status, 64); //read 64 bytes back.    

   std::cout<<std::endl;

   /* output status until a <CR> is found */
   
              
   for (i = 0; status[i] != 0xd; i++)
      {
      std::cout<<status[i];
      } 
   std::cout<<std::endl;

   
	/* close device and pipes */
	USBDRVD_PipeClose (pipe0);
	USBDRVD_PipeClose (pipe1);
	USBDRVD_CloseDevice (dev1);
	}

//}

    return;   
}
