"""
Live Feedback for Optimizing Exposure and Gain

Original Code from https://github.com/radioxoma/micromanager-samples
mm_live_video_controls.py

Edited January 2020 by Nick Schnoor for use with Thorlabs USB camera
"""

###############################################################################

MicroManagerDirectory="C:\Micro-Manager-2.0gamma"

###############################################################################

from pylab import *
ion()
import matplotlib.pyplot as plt
import os
import numpy as np
import time
import cv2
#import Qt
b=os.getcwd()
os.chdir(MicroManagerDirectory)
import MMCorePy
mmc = MMCorePy.CMMCore()

###############################################################################

c=[]

width, height = 320, 240
camtype="ThorlabsUSBCamera"
a=str(mmc.getAvailableDevices(camtype))
# DEVICE = ['Camera', 'OpenCVgrabber', 'OpenCVgrabber']
# DEVICE = ['Camera', "BaumerOptronic", "BaumerOptronic"]
"""

def set_mmc_resolution(mmc, width, height):
    Select rectangular ROI in center of the frame.

    x = (mmc.getImageWidth() - width) / 2
    y = (mmc.getImageHeight() - height) / 2
    mmc.setROI(x, y, width, height)
"""

def main():
    """Looping in function should be faster than in global scope.
    """
    fig=plt.figure()
    mmc = MMCorePy.CMMCore()
    mmc.enableStderrLog(False)
    mmc.enableDebugLog(False)
    #qt.QString.leftJustified(truncate=false)
    # mmc.setCircularBufferMemoryFootprint(100)
    os.chdir(MicroManagerDirectory)
    mmc.loadDevice("cam",camtype,a[2:-3])
    os.chdir(b)
    mmc.initializeDevice("cam")
    mmc.setCameraDevice("cam")
 
    cv2.namedWindow('MM controls')
    #cv2.resizeWindow('MM controls', 500, 100)
    if mmc.hasProperty("cam", 'HardwareGain'):
        hg=cv2.createTrackbar(
            'HardwareGain', 'MM controls',
            int(float(mmc.getProperty("cam", 'HardwareGain'))),
            int(mmc.getPropertyUpperLimit("cam", 'HardwareGain')),
            lambda value: mmc.setProperty("cam", 'HardwareGain', value))
        cv2.setTrackbarPos(hg,'MM controls',0)
    if mmc.hasProperty("cam", 'Exposure'):
        ex=cv2.createTrackbar(
            'Exposure [ms]', 'MM controls',
            int(float(mmc.getProperty("cam", 'Exposure'))),
        100,  # int(mmc.getPropertyUpperLimit("cam", 'Exposure')),
            lambda value: mmc.setProperty("cam", 'Exposure', float(value/30.)))
        cv2.setTrackbarPos(ex,'MM controls',1)
    
    if mmc.hasProperty("cam", 'FPS'):
        ex=cv2.createTrackbar(
            'FPS', 'MM controls',
            int(float(mmc.getProperty("cam", 'FPS'))),
             int(mmc.getPropertyUpperLimit("cam", 'FPS')),
            lambda value: mmc.setProperty("cam", 'FPS', int(value)))
        cv2.setTrackbarPos(ex,'MM controls',2)        
    #set_mmc_resolution(mmc, width, height)
   # cv2.namedWindow('Video')
    mmc.startContinuousSequenceAcquisition(1)
    while True:
        remcount = mmc.getRemainingImageCount()
        print('Images in circular buffer: %s') % remcount
        if remcount > 0:
            # rgb32 = mmc.popNextImage()
            rgb32 = mmc.getLastImage()
            print 'Maximum Pixel Value: ' + str(np.amax(rgb32))
            bgr = rgb32.view(dtype=np.uint8).reshape(
                rgb32.shape[0], rgb32.shape[1], 1)[..., :3]
            cv2.resizeWindow('MM controls', 1000, 800)
            cv2.imshow('MM controls', bgr)
            c.append(np.sum(rgb32)/(1290240))
            d=np.fft.fft2(rgb32);
            plt.cla()
            plt.plot(mean(d,1));
            fig.canvas.draw()
            
            #time.sleep(1)
        else:
            print('No frame')
        if cv2.waitKey(1) & 0xFF == ord('q'):           #click image, press 'q' to quit
            break

    cv2.destroyAllWindows()
    mmc.stopSequenceAcquisition()
    mmc.unloadAllDevices()
    mmc.reset()


if __name__ == '__main__':
    main()