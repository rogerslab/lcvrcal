%% LCVR Automated Collabration
% Written by Aaron Woods with help from J.D. Roger's Lab
% July 17, 2018

%% Set up/initialise PowerMeter

try
    ophirApp = actxserver('OphirLMMeasurement.CoLMMeasurement');
catch COM_error
    disp(COM_error.message);
    error('Could not establist a link to OphirLMMeasurement');
end
SerialNumbers = ophirApp.ScanUSB;
if(isempty(SerialNumbers))
    warndlg('No USB devices seem to be connected. Please check and try again',...
            'Ophir Measurement COM interface: ScanUSB error')
end
h_USB = ophirApp.OpenUSBDevice(SerialNumbers{1});
[Sensor_SN, Sensor_Type, Sensor_Name ]= ophirApp.GetSensorInfo(h_USB(1),0);
ophirApp.StartStream(h_USB(1),0);


%% Data Collection

voltages = [0:0.25:1,1.02:0.02:3, 3.25:0.25:10]; % volatges to test
mv = voltages*1000;
intensities=zeros(1,length(voltages));

 



%SET POWERMETER WAVELENGTH ON DEVICE




Temperature = 22; %Celsius
tempset= round((Temperature + 273.15) * 65535/500);

% RoomTemp=splitlines(LCVRusbver('tmp:1,?')); %Formatting Data Type from C++ code
% RoomTemp=str2double(cell2mat(extractAfter(RoomTemp(1),6))) %More Formatting
% tempset=[RoomTemp tempset]

% j=1;
% 
% LCVRcmd2(['tsp:1,' num2str(tempset(j))]);

% Temp Check for range +/- ~1/13 degree celsius
% TempQuery=0;
% while (TempQuery < tempset(j)-10 || TempQuery > tempset(j)+10) 
%     LCVRusbver(['tsp:1,' num2str(tempset(j))]);
%     pause(3);
%     TempQuery=splitlines(LCVRusbver('tmp:1,?')); %Formatting Data Type from C++ code
%     TempQuery=str2double(cell2mat(extractAfter(TempQuery(1),6))); %More Formatting
% end
% TempQuery=(TempQuery*500/65535)-273.15;


LCVRcmd2('inv:1,0'); % sets LCVR voltage to zero
pause(10)

nSamples = 5;
[Value, Timestamp, Status]= ophirApp.GetData(h_USB(1),0);
pause(0.5)

 for i=1:length(mv)
     valueLog = zeros(1, nSamples);
     LCVRcmd2(['inv:1,' num2str(mv(i))]); % sets LCVR voltage
     pause(1.5)
     for k = 1:nSamples
        [Value, Timestamp, Status]= ophirApp.GetData(h_USB(1),0);
        pause(0.4); %Allows time for data to be collected before it's stored
        if length(Value)>=1
            valueLog(k)= Value(end); %Only log the last sample
        else
            disp('No Data Collected for k=' + string(k))
        end
     end
     intensities(i)=mean(nonzeros(valueLog));
 end
figure(3)
   plot(voltages, intensities)
%% Perform tidying up actions and close PowerMeter:

ophirApp.StopAllStreams;
ophirApp.CloseAll;
ophirApp.delete;
clear ophirApp


%% Save
 lambda = 633;
 angle = 0;
 source='SuperK'; 
 emissionpower=0.20;
 save(['LCVRxpoldataSUPERK_' , num2str(lambda) , '_' , num2str(angle)  , '.mat'], 'lambda', 'angle', 'voltages', 'intensities', 'source', 'emissionpower')
