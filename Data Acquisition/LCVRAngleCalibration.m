%% LCVR Automated Collabration
% Written by:
% Aaron Woods July 17, 2018
% edited by:
% Ryan C. Niemeier 06/03/19 for new camera (IDS) and version change of micromanager
% Nick Schnoor April 25, 2020 to include temp control and co/cross pol
% Jeremy Rogers May 5, 2020 code cleanup and multiple runs, check std of phase
%

%% Set parameters

% voltage values to acquire
hwguess = 2.500;
voltages = 1*[0:.05:(hwguess-.05), (hwguess-.05+.005):.005:(hwguess+.05-.005), (hwguess+.05):.05:10];
mv = voltages*1000; % LCVR controller takes mV

% wavelength
lambda = 632.8;

% temperature (list in degrees C, or [] to skip setting temp and leave it as ambient)
tempsetpts = [24];

LCVRcmd(['tsp:1,' num2str(uint16((tempsetpts(1)+273.15)*65535/500))]);

% angle (manually adjust mount, this is only for record keeping
theta0 = -7; %Mounting offset
thetas = [-5:1:4 5:5:40 41:1:50];

% save data, use string for filename prefix or 0 for no saving
savename = 'angles'; %'angles'; %'temps'; % filename string for saving, i.e. "tempuratures" or 0 for not saving


%% Acquire data
% Initilize Camera
import mmcorej.*;
javaaddpath('MMcoreJ.jar')
camera_type='ThorlabsUSBCamera';
handles=camera_handler_development(camera_type);
handles.core.setExposure(.5);
exposure=handles.core.getExposure();
disp('Camera On')

% Grabs an image and init data array size
handles.core.snapImage();
width=handles.core.getImageWidth();
height=handles.core.getImageHeight();

if 0 % collect a dark frame for noise reduction
    disp('Acquiring dark frame, block beam and hit any key..'),pause()
    handles.core.snapImage(); dark = reshape(typecast(handles.core.getImage(),'uint8'),[width,height])';
    disp('Dark frame acquired, hit any key to continue'),pause()
end
cam_frames = zeros(height, width, length(mv));
data = zeros(width,length(mv),size(tempsetpts,2)+1);
exp=zeros(1,length(mv));

for ai=1:size(thetas,2) % loop over angles
    if size(thetas,2)>1 % only do this if thetas is non-empty
        disp(['set LCVR rotation mount to ' num2str(thetas(ai)+theta0) ' and hit enter'])
        pause
    end
    
    
    % zero the voltage and wait for it to settle (100ms should be enough), but bounce once to be sure)
    LCVRcmd('inv:1,0.0'); pause(0.10); LCVRcmd('inv:1,100');pause(0.10); LCVRcmd('inv:1,0.0'); pause(0.10);
    % get current temperature
    status = LCVRcmd('tmp:1,?'); currenttempC =(str2num(status(7:7+5))*500/65535-273.15);
    
    while abs(currenttempC-tempsetpts)>0.1 % set temp and wait until temp is reached
        pause(2)
        status = LCVRcmd('tmp:1,?'); currenttempC = (str2num(status(7:7+5))*500/65535-273.15);
    end
    
    measuredtempspre(ai) = currenttempC;
    for i=1:length(mv)
        LCVRcmd(['inv:1,' num2str(mv(i))]);% sets LCVR voltage
        disp([num2str((i+(ai-1)*length(mv))/...
            (size(mv,2)*size(thetas,2))*100) ' % done']);
        pause(0.1)
        handles.core.snapImage(); % take a shot
        tmp=typecast(handles.core.getImage(),'uint8');
        
        % check and adjust exposure, while keeping track
        high=max(max(tmp));
        high_max = 255; high_min = 150;
        exposure=handles.core.getExposure();
        while high==high_max || high < high_min
            if high==high_max
                disp('camera saturated, lowering exposure')
                handles.core.setExposure(0.9*exposure)
            else
                if high < high_min
                    disp('camera underexposed, raising exposure')
                    handles.core.setExposure(1.1*exposure)
                end
            end
            if exposure >= 475.0 || exposure <= 0.0095
                print('Exposure Limit Reached');
                break
            end
            handles.core.snapImage(); % take a shot
            tmp=typecast(handles.core.getImage(),'uint8');
            high=max(max(tmp));
            exposure=handles.core.getExposure();
        end
        
        % w/o reshape, image saves with dimensions "307200x1"
        tmp=reshape(tmp,[width,height])';
        cam_frames(:,:,i) = tmp;
        exp(i)=exposure;
    end
    status = LCVRcmd('tmp:1,?'); currenttempC = (str2num(status(7:7+5))*500/65535-273.15);
    measuredtempspost(ai) = currenttempC;
    data(:,:,ai)=squeeze(mean(double(cam_frames),1));
end
%close the camera

handles.core.unloadAllDevices(); % unload your device
LCVRcmd(['tsp:1,0'])
measuredtemps = (measuredtempspre+measuredtempspost)/2;


%% Plots Callibration Data interferometer

figure(1)

subplot(311) % Fringes
plot(1:size(data,1),data(:,:,1))
subplot(312) % Peak
fftdata = fft(data,size(data,1),1);
plot(1:size(data,1),abs(fftdata(:,:,1)))
subplot(313) % Retardance
hold on
% find the peak to determine the number of fringes (minus 1 because first
% point in FFT is the constant)
Nfringes = find(mean(abs(fftdata(:,:,1)),2)==max(mean(abs(fftdata(3:end/2,:,1)),2)),1)-1;
phase = squeeze(unwrap(atan2(imag(fftdata(Nfringes+1,:,:)),real(fftdata(Nfringes+1,:,:)))));
nmphase = phase*lambda/(2*pi);
xlabel('Voltage (V)')
ylabel('Retardence (nm)')
for i=1:length(thetas)
    plot(voltages, nmphase(:,i))
end
d=zeros(1,length(voltages)-1);
for t=1:length(voltages)-1
    d(t)=(nmphase(thetas==45,t+1)-nmphase(thetas==45,t))./(voltages(t+1)-voltages(t));
end
[~,ix]=max(abs(d));
halfwavevoltage=(voltage(ix)+voltage(ix+1))/2;
%%
if savename
    save([datestr(now,'yyyymmdd_HHMMSS') '_LCVRcal_' num2str(lambda) 'nm_' savename '.mat'], ...
        'cam_frames',...
        'data',...
        'exposure',...
        'lambda',...
        'measuredtemps',...
        'measuredtempspre',...
        'measuredtempspost',...
        'voltages',...
        'temps')
end
clear tmp
