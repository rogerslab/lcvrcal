/* LCVRusbver.cpp  
 * Meadowlark Optics D5020 LCVR controller
 * written by J.D. Rogers <jdrogers5 (at) wisc.edu>
 * June 27, 2018
 *
 * Based heavily on example cpp code from MLO
 *
 * usage:
 *
 */

#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <iostream>
#include <windows.h>
#include "usbdrvd.h"


/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],int nrhs, const mxArray*prhs[] )
{   
    
    
class mystream : public std::streambuf
{
protected:
virtual std::streamsize xsputn(const char *s, std::streamsize n) { mexPrintf("%.*s", n, s); return n; }
virtual int overflow(int c=EOF) { if (c != EOF) { mexPrintf("%.1s", &c); } return 1; }
};
class scoped_redirect_cout
{
public:
	scoped_redirect_cout() { old_buf = std::cout.rdbuf(); std::cout.rdbuf(&mout); }
	~scoped_redirect_cout() { std::cout.rdbuf(old_buf); }
private:
	mystream mout;
	std::streambuf *old_buf;
};
scoped_redirect_cout mycout_redirect;

//static scoped_redirect_cout mycout_redirect;

    
    
    /****************************************************************/   
    /* Check arguments                                              */
//     if (!(nrhs == 1) || !(nlhs==0) ) {   
//         mexErrMsgTxt("returnval = LCVRusbver()"); 
//     }  
    
//     // Init camera and get handle 
//     int nRet = 0;
//     plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS,mxREAL); //camera handle is 1st output
//     HCAM *pcam  = (HCAM *)mxGetPr(plhs[0]); 
//     nRet = is_InitCamera(pcam,NULL);
//     if (nRet != IS_SUCCESS) 
//     {
//        printf("error code: %d \n",nRet);
//         mexErrMsgTxt("Error opening camera: Not connected or already open"); 
//     }
//     HCAM hCam = *pcam;

/************************************************************************
*
*                               Meadowlark Optics
*                               Copyright (2006-2017)
*
* File name: usb_ver.cpp
*
* Description: This file contains sample source code that will initialize
*              a USB connection to a Meadowlark Optics D5020, send a
*              ver:? command and read the status response.
*
************************************************************************/


// void main()
// {
#define  flagsandattrs  0x40000000
       

BYTE ver_cmd [] = {'v', 'e', 'r', ':', '?', '\n'};
BYTE status [64];
INT cmd_size = 6; //The size of the byte array of the command.
HANDLE dev1, pipe0, pipe1;

UINT devcnt, i;
UINT USB_PID;

GUID  theGUID;

theGUID.Data1 = 0xa22b5b8b;
theGUID.Data2 = 0xc670;
theGUID.Data3 = 0x4198;
theGUID.Data4 [0] = 0x93;
theGUID.Data4 [1] = 0x85;
theGUID.Data4 [2] = 0xaa;
theGUID.Data4 [3] = 0xba;
theGUID.Data4 [4] = 0x9d;
theGUID.Data4 [5] = 0xfc;
theGUID.Data4 [6] = 0x7d;
theGUID.Data4 [7] = 0x2b;

USB_PID = 0x139C; //PID for the D5020.

devcnt = USBDRVD_GetDevCount (USB_PID);

if (devcnt == 0)
   {
	std::cout<<"No Meadowlark Optics USB Devices Present."<<std::endl;
   }
else
	{
	/* open device and pipes */
	dev1 = USBDRVD_OpenDevice (1, flagsandattrs, USB_PID);
	pipe0 = USBDRVD_PipeOpen (1, 0, flagsandattrs, &theGUID);
	pipe1 = USBDRVD_PipeOpen (1, 1, flagsandattrs, &theGUID);

	/* send ver:? command */
	USBDRVD_BulkWrite (dev1, 1, ver_cmd, cmd_size);

   /* read status response */
	USBDRVD_BulkRead (dev1, 0, status, 64); //read 64 bytes back.

   std::cout<<std::endl;

   /* output status until a <CR> is found */
   for (i = 0; status[i] != 0xd; i++)
      {
      std::cout<<status[i];
      }

   std::cout<<std::endl;

	/* close device and pipes */
	USBDRVD_PipeClose (pipe0);
	USBDRVD_PipeClose (pipe1);
	USBDRVD_CloseDevice (dev1);
	}
//}

    return;   
}
