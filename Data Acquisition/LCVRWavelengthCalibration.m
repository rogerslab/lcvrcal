%% LCVR Automated Collabration
% Written by:
% Aaron Woods July 17, 2018
% edited by:
% Ryan C. Niemeier 06/03/19 for new camera (IDS) and version change of micromanager
% Nick Schnoor April 25, 2020 to include temp control and co/cross pol
% Jeremy Rogers May 5, 2020 code cleanup and multiple runs, check std of phase
%

%% Set parameters

% voltage values to acquire
hwguess = 2.700; %approximate voltage for half wave, use finer sampling around this point to get accurate value
voltages = [0:.05:(hwguess-.2), (hwguess-.2+.005):.005:(hwguess+.2-.005), (hwguess+.2):.05:10]; %*0+4;
mv = voltages*1000; % LCVR controller takes mV

% wavelength
lambda = [500:10:650];

% temperature (list in degrees C, or 0 to skip setting temp and leave it as ambient)
tempsetpts = [24]; %[25:5:50];
LCVRcmd(['tsp:1,' num2str(uint16((tempsetpts(1)+273.15)*65535/500))]);

% angle (manually adjust mount, this is only for record keeping
theta0 = -6; % this is the value on the mount for fast axis @ 45deg
thetas = [0 45];


% save data, use string for filename prefix or 0 for no saving
% example: savefile =
savename = 'wavelengths'; %'angles'; %'temps'; % filename string for saving, i.e. "temperatures" or 0 for not saving


%% Acquire data
% Initilize Camera
import mmcorej.*;
javaaddpath('MMcoreJ.jar')
camera_type='ThorlabsUSBCamera';
handles=camera_handler_development(camera_type);
handles.core.setExposure(.5);
exposure=handles.core.getExposure();
disp('Camera On')

% Grabs an image and init data array size
handles.core.snapImage();
width=handles.core.getImageWidth();
height=handles.core.getImageHeight();

cam_frames = zeros(height, width, length(mv));
data = zeros(width,length(mv),size(tempsetpts,2),size(thetas,2));
exp=zeros(1,length(mv));

for ti=1:size(thetas,2) % loop over temperatures
    disp(['set LCVR rotation mount to ' num2str(thetas(ai)+theta0) ' and hit enter'])
    pause()
    for wi=1:size(lambda,2) % loop over angles
        % zero the voltage and wait for it to settle (100ms should be enough), but bounce once to be sure)
        LCVRcmd('inv:1,0.0'); pause(0.10); LCVRcmd('inv:1,100');pause(0.10); LCVRcmd('inv:1,0.0'); pause(0.10);
        % get current temperature
        status = LCVRcmd('tmp:1,?'); currenttempC = (str2num(status(7:7+5))*500/65535-273.15);
        while (abs(currenttempC - tempsetpts) > .1) % wait for temp to stabilize
            pause(2)
            status = LCVRcmd('tmp:1,?'); currenttempC = (str2num(status(7:7+5))*500/65535-273.15);
        end
        measuredtempspre(ti,wi) = currenttempC;
        for i=1:length(mv)
            LCVRcmd(['inv:1,' num2str(mv(i))]); disp(num2str(mv(i)));% sets LCVR voltage
            disp([num2str((i+size(mv,2)*(tw-1)+size(mv,2)*size(lambda,2)*(ti-1))/...
                (size(mv,2)*size(thetas,2)*(size(lambda,2)))*100) ' % done']);
            pause(0.1)
            handles.core.snapImage(); % take a shot
            tmp=typecast(handles.core.getImage(),'uint8');
            
            % check and adjust exposure, while keeping track
            high=max(max(tmp));
            high_max = 255; high_min = 150;
            exposure=handles.core.getExposure();
            while high==high_max || high < high_min
                if high==high_max
                    disp('camera saturated, lowering exposure')
                    handles.core.setExposure(0.9*exposure)
                else
                    if high < high_min
                        disp('camera underexposed, raising exposure')
                        handles.core.setExposure(1.1*exposure)
                    end
                end
                if exposure >= 475.0 || exposure <= 0.0095
                    print('Exposure Limit Reached');
                    break
                end
                handles.core.snapImage(); % take a shot
                tmp=typecast(handles.core.getImage(),'uint8');
                high=max(max(tmp));
                exposure=handles.core.getExposure();
            end
            
            % w/o reshape, image saves with dimensions "307200x1"
            tmp=reshape(tmp,[width,height])';
            cam_frames(:,:,i) = tmp;
            exp(i)=exposure;
        end
        status = LCVRcmd('tmp:1,?'); currenttempC = (str2num(status(7:7+5))*500/65535-273.15)
        measuredtempspost(ti) = currenttempC;
        data(:,:,ti,wi)=squeeze(mean(double(cam_frames),1));
    end
end


%close the camera
handles.core.unloadAllDevices(); % unload your device
% turn off heating
LCVRcmd(['tsp:1,0'])
measuredtemps = (measuredtempspre+measuredtempspost)/2;

%% Plots Callibration Data interferometer
figure(1)
subplot(311) % Fringes
plot(1:size(data,1),data(:,:,1,1))
subplot(312) % Peak
fftdata = fft(data,size(data,1),1);
plot(1:size(data,1),abs(fftdata(:,:,1,1)))
subplot(313) % Retardance
hold on
% find the peak to determine the number of fringes (minus 1 because first
% point in FFT is the constant)
for wi=1:length(lambda)
    for i=1:length(thetas)
    Nfringes = find(mean(abs(fftdata(:,:,i,wi)),2)==max(mean(abs(fftdata(3:end/2,:,i,wi)),2)),1)-1;
    phase(:,i,wi) = reshape(unwrap(atan2(imag(fftdata(Nfringes+1,:,i,wi)),real(fftdata(Nfringes+1,:,i,wi)))),size(data,2));
    nmphase(:,i,wi) = phase*lambda(wi)/(2*pi);
    end
end
xlabel('Voltage (V)')
ylabel('Retardence (nm)')
for i=1:length(thetas)
    plot(voltages, phase(:,thetas==0,i))
end
d=zeros(length(lambda),length(voltages)-1);
for wi=1:length(lambda)
for t=1:length(voltages)-1
    d(wi,t)=(phase(t+1,thetas==45,wi)-phase(t,thetas==45,wi))./(voltages(t+1)-voltages(t));
end
end
[~,ix]=max(abs(d),[],2);
halfwavevoltage=(voltage(ix)+voltage(ix+1))/2;
%%
if savename
    save([datestr(now,'yyyymmdd_HHMMSS') '_LCVRcal_' num2str(lambda) 'nm_' savename '.mat'], ...
        'cam_frames',...
        'data',...
        'exposure',...
        'lambda',...
        'measuredtemps',...
        'measuredtempspre',...
        'measuredtempspost',...
        'voltages',...
        'temps')
end
clear tmp
