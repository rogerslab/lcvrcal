%% LCVR Calibration Data Acquisition
%
% Written by:
% Aaron Woods July 17, 2018
% Edited by:
% Ryan C. Niemeier 06/03/19 for new camera (IDS) and version change of micromanager
% Nick Schnoor April 25, 2020 to include temp control and co/cross pol
% Jeremy Rogers May 5, 2020 code cleanup and multiple runs, check std of phase
% Nick Schnoor June 2020 Polishing for publication

%% Set values

% voltage values to acquire
hwGuess = 2.500; % Guess of what voltage gives a half wave of retardance.
% Changes slightly for each wavelength, but should be close enough for all as long as wavelengths stay in vis.
% May need to run this program multiple times to establish a suitable guess.

% Smaller voltage steps around half wave for finer accuracy:
%voltages=0:0.05:10;
voltages = [0:.05:(hwGuess-.2),(hwGuess-.2+.005):.005:(hwGuess+.2-.005),(hwGuess+.2):.05:10];
milliVoltages = voltages*1000; % LCVR controller takes mV

% Shorter voltage list around steepest range to more speedly find half wave
voltages45= voltages(find(round(voltages,3)==floor(hwGuess)):find(round(voltages,3)==ceil(hwGuess)));
milliVoltages45=voltages45*1000;

[~,vMatch,~]=intersect(voltages,voltages45);


% Wavelength(s)
lambdas = [530:10:650];

% Set temperature slightly above ambient
currentTemp = LCVRcmd('tmp:1,?'); 
currentTempC = (str2double(currentTemp(7:12))*500/65535-273.15);

tempSetC = [currentTempC+1.0]; %If want to set a manual temp, place  here in degrees C
                    % (or [] to skip setting temp and leave it as ambient)
                    
% Need data at 2 angles to determine halfwave voltage
angles = [45 0];
%% Acquire data

% Initilize Camera and test connection
import mmcorej.*;
javaaddpath('MMcoreJ.jar')
camera_type = 'ThorlabsUSBCamera';
handles = camera_handler_development(camera_type);
handles.core.setExposure(.5)
exposure = handles.core.getExposure();
disp('Camera On')

% Grabs an image and init data array size
handles.core.snapImage();
frameWidth=handles.core.getImageWidth();
frameHeight=handles.core.getImageHeight();

% Set Temperature (if any) and wait for the temperature to be reached
disp('Setting Temperature...');
if isempty(tempSetC) ~= 1
    tempSet = round((tempSetC + 273.15) * 65535/500);
    LCVRcmd(['tsp:1,' num2str(tempSet)]);
    currentTemp = LCVRcmd('tmp:1,?'); currentTempC = (str2double(currentTemp(7:12))*500/65535-273.15);
    count=1;
    while (abs(currentTempC - tempSetC) > .05 && count < 60)
        pause(1); count = count+1;
        currentTemp = LCVRcmd('tmp:1,?'); currentTempC = (str2double(currentTemp(7:12))*500/65535-273.15);
    end
end

nVoltages = length(milliVoltages);
nVoltages45 = length(milliVoltages45);
nLambdas = length(lambdas);
nAngles = length(angles);

% Preallocate Variables
camFrames = NaN(nVoltages,frameHeight,frameWidth);
data = NaN(nAngles,nLambdas,nVoltages,frameWidth);
fftData=data;
exposureArray = NaN(nAngles,nLambdas,nVoltages);
measuredTempsPre = NaN(nAngles,nLambdas);
measuredTempsPost = NaN(nAngles,nLambdas);

% Scan through all voltages for all wavelengths at both angles and get data
figure(1), hold on
for iAngle = 1:nAngles
    disp(['Rotate LCVR so Fast Axis is ',num2str(angles(iAngle)),' degrees above Horizontal. Press any key when ready.']),pause()
    
    if angles(iAngle)==45
        nV=nVoltages45;
        volts=milliVoltages45;
    else
        nV=nVoltages;
        volts=milliVoltages;
    end
    
    for jLambda = 1:nLambdas % Loop through Wavelengths
        disp(['Set Wavelength to ',num2str(lambdas(jLambda)),' nm. Press any key when ready.']),pause()
        % Zero the voltage and wait for it to settle (100ms should be enough), but bounce once to be sure)
        LCVRcmd('inv:1,0.0'); pause(0.10); LCVRcmd('inv:1,100');pause(0.10); LCVRcmd('inv:1,0.0'); pause(0.10);
        currentTemp = LCVRcmd('tmp:1,?'); currentTempC = (str2double(currentTemp(7:12))*500/65535-273.15);
        measuredTempsPre(iAngle,jLambda) = currentTempC;
        pause(10) % Wavelength Stabilization Time
        for kVoltage = 1:nV % Loop through Voltages
            LCVRcmd(['inv:1,' num2str(volts(kVoltage))]); disp(num2str(volts(kVoltage)));% sets LCVR voltage
            pause(0.2) % Crystal tilt transient time
            handles.core.snapImage(); % Take a shot
            tempImage = typecast(handles.core.getImage(),'uint8');
            
            % Check and adjust exposure, while keeping track
            % Avoid saturation while keeping good contrast
            maxIntensity = max(max(tempImage));
            saturationLimit = 255; contrastLimit = 150;
            exposure = handles.core.getExposure();
            count=1;
            while (maxIntensity == saturationLimit || maxIntensity < contrastLimit) && count < 60
                if maxIntensity == saturationLimit
                    disp('camera saturated, lowering exposure')
                    handles.core.setExposure(0.9*exposure)
                else
                    if maxIntensity < contrastLimit
                        disp('camera underexposed, raising exposure')
                        handles.core.setExposure(1.1*exposure)
                    end
                end
                if exposure >= 450.0 || exposure <= 0.0095
                    print('Exposure Limit Reached');
                    break
                end
                handles.core.snapImage(); % take a shot
                tempImage = typecast(handles.core.getImage(),'uint8');
                maxIntensity = max(max(tempImage));
                exposure = handles.core.getExposure();
                count=count+1;
            end
            
            % Without reshape, image saves with dimensions "307200x1"
            tempImage = reshape(tempImage,[frameWidth,frameHeight])';
            if angles(iAngle)==45
            camFrames(vMatch(kVoltage),:,:) = tempImage;
            exposureArray(iAngle,jLambda,vMatch(kVoltage)) = exposure;
            else
                        camFrames(kVoltage,:,:) = tempImage;
            exposureArray(iAngle,jLambda,kVoltage) = exposure;    
            end
        end
        % Retrieve temps before and after every scan for troubleshooting
        % Suspicious data
        currentTemp = LCVRcmd('tmp:1,?'); currentTempC = (str2double(currentTemp(7:12))*500/65535-273.15);
        measuredTempsPost(iAngle,jLambda) = currentTempC;
        % Store data
            data(iAngle,jLambda,:,:) = squeeze(mean(double(camFrames),2));
        fftData(iAngle,jLambda,:,:) = fft(data(iAngle, jLambda,:,:),frameWidth,4);
        % Find the peak to determine the number of fringes
        if angles(iAngle)==45
            fftDataMaxIx = find(mean(abs(fftData(iAngle,jLambda,vMatch,:))),3) == max(mean(abs(fftData(iAngle,jLambda,vMatch,3:end/2)),3),1);
        else
            fftDataMaxIx = find(mean(abs(fftData(iAngle,jLambda,:,:))),3) == max(mean(abs(fftData(iAngle,jLambda,:,3:end/2)),3),1);
        end
        % Radians to nm
        nmRetardance(iAngle,jLambda,:) = radRetardance(iAngle,jLambda,:)*lambdas(jLambda)/(2*pi);
        plot(volts/1000,reshape(nmRetardance(iAngle,jLambda,:),[],1))
    end
end
% Close the camera, stop LCVR heating
handles.core.unloadAllDevices(); % Unload your device
LCVRcmd('tsp:1,0')

%% Analyze Data and Plot

% fftData = fft(data,frameWidth,4);
% % Find the peak to determine the number of fringes
% fftDataMaxIx = find(mean(abs(fftData(1,1,:,:)),3) == max(mean(abs(fftData(1,1,:,3:end/2)),3)),1);
% nFringes=fftDataMaxIx-1; % First point in FFT is the constant (0 fringes)
% % Find phases of fringe patterns across the frames
% radRetardance = reshape(unwrap(atan2(imag(fftData(:,:,:,fftDataMaxIx+1)),real(fftData(:,:,:,fftDataMaxIx+1))),[],3),nAngles,nLambdas,nVoltages);
% % Radians to nm
% nmRetardance = radRetardance.*lambdas/(2*pi);

% Initial phase of fringes across the frame is arbitrary
% Need to fix a voltage/phase value somewhere to eliminate this ambiguity
% Use copol method in interferometer setup to determine voltage for half
%   wave retardance. Use this to fix the curves properly in phase-space
%   (ignoring retardance order --> still a +/- 2npi degeneracy)
retardanceSlope = NaN(nLambdas,nVoltages45);
angle45Ix = find(angles == 45);
angle0Ix = find(angles == 0);
hwVoltageIx=NaN(1,nLambdas);
for iLambda=1:nLambdas
    for smallVStepIxs = 1:nVoltages45-1
    % Half wave phase retardance when copol data has steepest movement
    % Max(d(retardance)/d(voltage))
    ret=rmmissing(squeeze(radRetardance(angle45Ix,iLambda,:)));
    retardanceSlope(iLambda,smallVStepIxs) = ...
        (ret(smallVStepIxs+1)-ret(smallVStepIxs))./(voltages45(smallVStepIxs+1)-voltages45(smallVStepIxs));
    end

[~,hwVoltageIx45] = max(abs(retardanceSlope(iLambda,:)),[],2);
hwVoltageIx(iLambda) = find(voltages==voltages45(hwVoltageIx45));
end
disp(['Half Wave Voltage seems to be around ',num2str(mean(voltages(hwVoltageIx))),' V'])
% figure(5)
% plot(lambdas, voltages(hwVoltageIx)) %Test 45 data. Should be linearish
[hwDispersionFit,hwRSqard]=fit(lambdas(1:end)',voltages(hwVoltageIx(1:end))','poly1'); %Omit bad 45 data with indices

% Approximate Colorbar for plots with accurate colors for vis regime lambdas
rgbRainbow = hsv(300);
rgbRainbow = flipud(rgbRainbow(1:251,:));

% Plotting of retardance vs voltage for all wavelengths in 2d with proper colors
figure(2)
set(gca,'FontName','Times New Roman')
hold on
nmRetardanceAdj = zeros(nLambdas,nVoltages);
lambdaColors = zeros(nLambdas,3);
for iLambda = 1:nLambdas
    % Shifing curves to fix arbitrary initial fringe phase
    % In case curve is inverted
    if nmRetardance(angle0Ix,iLambda,1) < nmRetardance(angle0Ix,iLambda,nVoltages)
        nmRetardanceAdj(iLambda,:) = -nmRetardance(angle0Ix,iLambda,:);
    else
        nmRetardanceAdj(iLambda,:) = nmRetardance(angle0Ix,iLambda,:);
    end
    hwOffset=interp1(voltages,nmRetardanceAdj(iLambda,:)',hwDispersionFit(iLambda));
    nmRetardanceAdj(iLambda,:)=nmRetardanceAdj(iLambda,:)-hwOffset+lambdas(iLambda)/2;
    
    
    % Assigning colors to the wavelengths
    if lambdas(iLambda) <= 400
        lambdaColors(iLambda,:) = rgbRainbow(1,:);
    elseif lambdas(iLambda) >= 650
        lambdaColors(iLambda,:) = rgbRainbow(251,:);
    else
        lambdaColors(iLambda,:) = rgbRainbow(round(lambdas(iLambda))-399,:);
    end
    plot(voltages,nmRetardanceAdj(iLambda,:),'Color',lambdaColors(iLambda,:))
end

xlabel('Voltage [V]','Fontsize',8)
ylabel(['Retardance of different Wavelengths [nm]'],'Fontsize',8)
title('Wavelength Dependence of LCVR Retardance')
hold off

colormap(rgbRainbow)
cb = colorbar;
cbLims = [400:50:650];
set(cb,'Ticks',linspace(0,1,6),'TickLabels',cbLims,'FontName','Times New Roman','Fontsize',6,'Position',[0.85,0.13,0.03,0.7]);
cb.Label.String = 'Laser Wavelength [nm]';
set(gcf,'PaperSize',[4,3],'PaperPosition',[0,0,4,3])
% print -dpdf LCVRWavelengthRetake.pdf
%% Surface Approximation and Plot

figure(3)
hold on
%Plotting retardance curves in 3d space-useful for comparison to surface fit
p = plot3((ones(nLambdas,1)*voltages)',ones(nVoltages,1)*lambdas,nmRetardanceAdj','Marker','o');
set(p,{'color'},num2cell(lambdaColors,2))

coeffs = zeros(nLambdas,5);
rSquareds = ones(nLambdas,1);
residuals = ones(nLambdas,nVoltages);

% Fitting each wavelength's retardance curve
for iLambda = 2:nLambdas
    ft = fittype('a+b/(1+(x/c)^d)^e',...
        'independent',{'x'},'dependent',{'y'},'coefficients',{'a','b','c','d','e'});
    opt = fitoptions(ft);
    % opt.Startpoint=[-2,12,2,4.6,0.568]; % rad
    opt.Startpoint = [-630,1000,1.6,8,0.25]; % nm
    
    [lambdaFit,gof] = fit(voltages',nmRetardanceAdj(iLambda,:)',ft,opt);
    fitRetardance = lambdaFit(voltages)';
    plot3(voltages,lambdas(iLambda)*ones(1,nVoltages),fitRetardance,'k','LineWidth',0.5)
    coeffs(iLambda,:) = coeffvalues(lambdaFit)';
    
    % Getting RSquareds and Residuals for troubleshooting awry surfaces
    q = cell2mat(struct2cell(gof));
    rSquareds(iLambda) = q(2);
    residuals(iLambda,:) = fitRetardance-nmRetardanceAdj(iLambda,:);
end

% Fitting b coefficient across wavelengths
% ---> Dispersion dependence of retardance curve (Approx 2nd order in vis)
ft2 = fittype({'1','1/x^2'});
[bFit,gof2] = fit(lambdas(2:end)',coeffs(2:end,2),ft2);
[aFit,gof3] = fit(lambdas(2:end)',coeffs(2:end,1),ft2);

% Plot to see if a and b fitting is ok:
figure(4)
bFitLambdas = [round(min(lambdas))-20:round(max(lambdas))+20];
bFitValues = bFit(bFitLambdas);
plot(lambdas(2:end),coeffs(2:end,2),bFitLambdas,bFitValues);
title('Fitting of the b Parameter');
legend('b Values from Wavelength Fits','Fit of b across Wavelengths');

figure(5)
aFitLambdas = [round(min(lambdas))-20:round(max(lambdas))+20];
aFitValues = aFit(aFitLambdas);
plot(lambdas(2:end),coeffs(2:end,1),aFitLambdas,aFitValues);
title('Fitting of the a Parameter');
legend('a Values from Wavelength Fits','Fit of a across Wavelengths');

bs = coeffvalues(bFit);
as = coeffvalues(aFit);

% Calculating values and plotting the 3d surface fit over the data
%   using our determined coefficients
voltagesForSurf = [0:0.1:10];
lambdasForSurf = bFitLambdas;

a = as(1)+as(2)./lambdasForSurf.^2;
b = bs(1)+bs(2)./lambdasForSurf.^2;

meancoeffs = mean(coeffs(2:end,:),1);
c = meancoeffs(3);d = meancoeffs(4);e = meancoeffs(5);
% Displaying coefficients
disp(['a1= ',num2str(as(1)),'   a2= ',num2str(as(2))]);
disp(['b1= ',num2str(bs(1)),'   b2= ',num2str(bs(2))]);
disp(['c= ',num2str(c)])
disp(['d= ',num2str(d)])
disp(['e= ',num2str(e)])

surfVals = a' + b'./(1+(voltagesForSurf./c).^d).^e;
figure(3)
surf(voltagesForSurf,lambdasForSurf,surfVals)
colormap(bone)

set(gca,'FontName','Times New Roman')
xlabel('Voltage [V]','FontSize',8)
ylabel('Wavelength [nm]','FontSize',8)
zlabel('Retardance [nm]','FontSize',8)
title('Surface Fit of Wavelength Dependence of Retardance ','FontSize',10)
set(gcf,'PaperSize',[4,3],'PaperPosition',[0,0,4,3])

view(60,15)
% shading interp % Controls contour lines on plot (can get busy)

%% Evaluating residuals of surface fit vs data
% Residuals are useful to see goodness of the surface approximation

bValsForResiduals = bs(1)+bs(2)./lambdas.^2;
aValsForResiduals = as(1)+as(2)./lambdas.^2;
surfValsForResiduals = aValsForResiduals' + bValsForResiduals'./(1+(voltages./c).^d).^e;

surfResiduals = nmRetardanceAdj(3:end,:)-surfValsForResiduals(3:end,:);
disp(['Mean of Absolute Residuals between Data and Surface Approximation: ',num2str(mean(abs(surfResiduals),'all')),' nm'])
disp(['Max of Absolute Residuals between Data and Surface Approximation: ',num2str(max(abs(surfResiduals),[],'all')),' nm'])
