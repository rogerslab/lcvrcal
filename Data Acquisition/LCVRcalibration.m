 %% LCVR Automated Collabration
% Written by Aaron Woods with help from J.D. Roger's Lab
% July 17, 2018
% edited RCN 06/03/19 for new camera (IDS) and version change of
% micromanager
%%
savefile = 1; % 1 for saving, 0 for not saving

%% Initiates Camera
import mmcorej.*;            
javaaddpath('C:\Users\schno\Documents\Rogers Lab\LCVR\MMcoreJ.jar')

camera_type='ThorlabsUSBCamera';
handles=camera_handler_development(camera_type)
disp('Camera On')

%% Grabs Images from the Selected Camera
handles.core.snapImage();
width=handles.core.getImageWidth();
height=handles.core.getImageHeight();

%%
%voltages = [0:.25:6.60, 6.62:0.02:7.28, 7.30:0.10:10];
voltages = [0:0.1:1.0, 1.05: 0.05: 3.00, 3.10:0.10:6.5, 6.55:0.05:7.45, 7.5:0.1:10.0];% volatges to test
mv = voltages*1000;

lambda = 675;

% set temperature here
% Temperature = input('Temperature Set Point (Celsius):');
% tempset= round((Temperature + 273.15) * 65535/500)
% LCVRcmd2(['tsp:1,' num2str(tempset)]);
% 

cam_frames = zeros(height, width, length(mv));
exp=zeros(1,length(mv));
%      % Temp Check for range +/- ~1/13 degree celsius
%      TempQuery=0;
%      while (TempQuery < tempset-10 | TempQuery > tempset+10) 
%         LCVRcmd2(['tsp:1,' num2str(tempset)]);
%         pause(1);
%         TempQuery=splitlines(LCVRcmd2('tmp:1,?')); %Formatting Data Type from C++ code
%         TempQuery=str2double(cell2mat(extractAfter(TempQuery(1),6))); %More Formatting
%      end
% 
LCVRcmd2('inv:1,0.0');
pause(2.0);
for i=1:length(mv)
     
    
     LCVRcmd2(['inv:1,' num2str(mv(i))]); % sets LCVR voltage
     pause(0.8)
     handles.core.snapImage(); % take a shot
     %hmm Sony camera is 8bit.. 
%     if strcmp(camera_type,'Sony')
%     %if strcmp(camera_type,'IDS')
         tmp=typecast(handles.core.getImage(),'uint8'); 
%     else
%        tmp=typecast(handles.core.getImage(), 'uint16');
%     end
%   if i==1
    high=max(max(tmp));
         while high==255 || high < 100
             exposure=handles.core.getExposure();
             if high==255
                 disp('camera saturated, lowering exposure')
                handles.core.setExposure(0.95*exposure)
             else
                if high < 50
                    disp('camera undersaturated, raising exposure')
                    handles.core.setExposure(1.05*exposure)

                end
             end
             if exposure >= 475.0 || exposure <= 0.0095
                 print('Exposure Limit Reached');
                 break
             end
             handles.core.snapImage(); % take a shot
             tmp=typecast(handles.core.getImage(),'uint8');
             high=max(max(tmp));
         end
         
    % w/o reshape, image saves with dimensions "307200x1"
    tmp=reshape(tmp,[width,height])';
    cam_frames(:,:,i) = tmp;
    data(:,i)=mean(double(tmp),1);
    exp(i)=exposure;
 end
 
 %%
%closes the camera
handles.core.unloadAllDevices(); % unload your device

%% Animated Graph 
% Use for Debugging
if 0
figure(1)
    for i=1:size(data,3)
     plot(1:width,squeeze(mean(data(:,:,i),1)))
     title(num2str(voltages(i)))
     drawnow
     pause(.1)
    end
end    
%% Fourier Transform
fftdata = fft(data,size(data,1),1);

% %% Comparasion to Data Recieved by Meadowlark
% 
% % Open the file for reading, and obtain the file identifier, fileID.
%     filename = 'LC# G14087.txt';
%     fileID = fopen(filename);%,'r');
%     
% % Reads file data into an array. Reads in 3 floating point numbers
% % and skips 7 headerlines.
%     data2 = textscan(fileID,'%f%f%f','headerlines',7);
% 
% % Make sure to close the file once you are done reading the data
%     fclose(fileID);
%     
% % Creates the graph  
%     figure(4)
%     plot(cell2mat(data2(1)),cell2mat(data2(2)))
%     title('Data Recieved by Meadowlark')
%     xlabel('Voltage (mV)')
%     ylabel('Retardence (nm)')
%% Plots Callibration Data interferometer

figure(3)

subplot(311)
plot(1:size(data,1),data)
subplot(312)
plot(1:size(data,1),abs(fftdata))
xlim([0 30])
subplot(313)  
Nfringes = find(mean(abs(fftdata),2)==max(mean(abs(fftdata(3:end/2,:)),2)),1);
phase = unwrap(atan2(imag(fftdata(Nfringes,:)),real(fftdata(Nfringes,:))));
nmphase = phase*lambda/(2*pi);
plot(voltages,nmphase,'+-')   
%plot(cell2mat(data2(1)),cell2mat(data2(2)))
hold on; 
%plot(voltages*10^3,nmphase+(data2{2}(1)-nmphase(1)),'+-')
% std(nmphase)
% plot(cell2mat(data2(1)),cell2mat(data2(2)),'-r',voltages*10^3,nmphase+(data2{2}(1)-nmphase(1)),'-b')
% hold off
legend(num2str(lambda))
    xlabel('Voltage (mV)')
    ylabel('Retardence (nm)')
figure(4)
plot(voltages, nmphase)
title(['Nfringes = ' num2str(Nfringes)])
%xlim([7,10])
%% XPOl
intensity=reshape(mean(cam_frames,[1,2]),[1,length(voltages)])./exp;
norm_intensity=(intensity-min(intensity))/(max(intensity)-min(intensity));
phs=real(2*acos(sqrt(norm_intensity)))/(pi);
figure(5)
plot(voltages, phs)


%%
%  save(['LCVRcal' num2str(lambda)],'data','fftdata','voltages','nmphase','phase','lambda')
if savefile
    cd('C:\Users\schno\Documents\Rogers Lab\LCVR\Interferometer')
    fname = 'Wavelength';%input('write filename as string:');
    ftime = clock;
    %if exist('cam_frames')
    save([fname, '_LCVRcal_',num2str(lambda), 'nm'])%,'data','fftdata','voltages','nmphase','phase','lambda', 'ftime', 'cam_frames', 'Nfringes')
%     else
%     save([fname, '_LCVRcal_',num2str(lambda), 'nm_',num2str(Temperature), 'C'],'data','fftdata','voltages','nmphase','phase','lambda', 'ftime', 'Temperature')
disp('data saved')
end


clear tmp
